FROM java:8-alpine
MAINTAINER Your Name <you@example.com>

ADD target/uberjar/com.toryanderson.jar /com.toryanderson/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/com.toryanderson/app.jar"]
