(ns com.toryanderson.handler
  (:require [compojure.core :refer [routes wrap-routes]]
            [com.toryanderson.layout :refer [error-page]]
            [com.toryanderson.routes.home :refer [home-routes]]
            [compojure.route :as route]
            [com.toryanderson.env :refer [defaults]]
            [mount.core :as mount]
            [com.toryanderson.middleware :as middleware]))

(mount/defstate init-app
                :start ((or (:init defaults) identity))
                :stop  ((or (:stop defaults) identity)))

(def app-routes
  (routes
    (-> #'home-routes
        (wrap-routes middleware/wrap-csrf)
        (wrap-routes middleware/wrap-formats))
    (route/not-found
      (:body
        (error-page {:status 404
                     :title "page not found"})))))


(defn app [] (middleware/wrap-base #'app-routes))
