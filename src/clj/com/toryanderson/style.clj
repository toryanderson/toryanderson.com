(ns com.toryanderson.style
  (:require [garden.def :refer [defstylesheet defstyles defkeyframes]]
            [garden.units :as u :refer [px]]
            [garden.color :as c :refer [hex->hsl hsl->hex]] ;:rename {hex->rgb hr, rgb->hex rh}]
            [garden.selectors :as s :refer [nth-child]]
            [clojure.string :as str]))
(def tep-charsheet
  [:.tep-charsheet
   [:input {:width "3em"
              :border-style "solid"
              :border-width "1px"
            :border-color "black"}]
   [:div.table {:display "table"
                :border-style "solid"
                :border-width "1px"
                :border-color "#111"}
    [:.row {:display "table-row"}]
    [:.col {:display "table-cell"
            :border-style "solid"
            :border-width "1px"
            :border-color "#111"}]]
   [:.personaldetails
    [:.detail {:display "inline-block"
               :margin-right "1.5em"}
     [:label {:width "5em"
              :font-weight "600"}]]
    [:.detail.detail_Name :.detail.detail_Race {:display "block"
                           :width "100%"}
     [:label {:position "relative"
              :display "inline-block"}]
     [:input {:width "30em"}]]]
   [:.points {:display "block"
              :width "100%"
              ;:text-align "center"
              }
    [:div {:display "inline-block"
           :width "25%"}
     [:label {:width "3em"
              :font-weight "600"
              :display "block"}]
     [:input {:width "3em"
              :height "3em"}]
     
     ]]])


;;;;;;;;;;;;;;;;;;;;
;; COLOR PALLETTE ;;
;;;;;;;;;;;;;;;;;;;;
(def tsa-colors
  {:gold-light "#fff0cb"
   :gold-dark "#746442"
   :silver-light "#e7e1d5"
   :silver-dark "#524d47"})

(def nav-color {:text (tsa-colors :gold-dark)
                :hover (tsa-colors :silver-dark)})

(def nav-height 100)

;;;;;;;;;;;;
;; STYLES ;;
;;;;;;;;;;;;
(defn gradient [color1 color2]
  "Takes two color hex-strings, returns the 'linear-gradient' command that can be put into a :background element"
  (str "linear-gradient(" color1 ", " color2 ")")
  )

;; TODO make multimethod for gradients
(defn rgradient [& colors]
  (str "radial-gradient(" (str/join ", " colors) ")"))

(defstyles main
  {:output-to "resources/public/css/style.css"
   :vendors ["webkit" "moz" "o" "ms"]} ;; Not working with defstyles (must use (apply garden.core/css main))

  [:body
   { ;:background-color (tsa-colors :silver-light)
    :background (gradient
                 (tsa-colors :silver-light)
                 (tsa-colors :silver-dark))
    :background-repeat :no-repeat
    :background-attachment :fixed
    :font-size (px 16)
    :line-height 1.5}
   [:div#app
    [:.container
     {:border-color (tsa-colors :silver-dark)
      :border-width "2px"
      :border-style :solid
      :padding "8%"
      :background (rgradient "white"
                             (tsa-colors :silver-light)
                             (tsa-colors :silver-dark))}]]]
  [:div#navbar :nav.navbar {:background (gradient
                                         (tsa-colors :gold-dark)
                                         (tsa-colors :gold-light))
                                        ;:background-color (tsa-colors :gold-light)
                            }
   [:a.navbar-brand :a.nav-link {:color (tsa-colors :gold-dark)
                                 :margin "0 5%"}
    [:img {:height (px nav-height)}]
    [:&:hover {:color "inherit"}]]
   [:ul.nav.navbar-nav [:li.nav-item
                        [:.nav-link {:color (nav-color :text)
                                     :display "inline-block"}]
                        [:a.nav-link:hover {:color (nav-color :hover)}]
                        [:.username {:display "inline-block"
                                     :padding-left "0.5em"
                                     ;:color (ycolors :lightestblue)
                                     }]]]
   [:.dropdown-menu
    {:background (rgradient "white"
                            (tsa-colors :gold-light)
                            )
     :padding "2px 8px"}
    [:a {:color (tsa-colors :gold-dark)}]]]
  
  ;[:.navbar-inverse [:.navbar-nav [:> [:.active [:> [:a (nav-plain-blue)]]]]]]
  ;[:.navbar-inverse [:.navbar-nav [:> [:.active [:> [:a:hover (nav-hover-blue)]]]]]]

  [:.jumbotron {:background (rgradient (tsa-colors :gold-light) (tsa-colors :gold-dark));"rgba(0, 10, 30, .1)"
                                        ;:color (ycolors :lightestblue)
                ;; TODO corners
                :color (tsa-colors :gold-dark)
                :text-align "center"
                :border-style "groove"
                }]

  [:div.doc {:display "inline-block"
             :margin (px 3)}]
  [:div#funding-options {:text-align "center"}
   [:a.btn {:display "block"
            :width "20%"
            :margin "0.5em auto"}]]
  [:.clear {:clear "both"}]

  
  [:h2.organization {:font-style "italic"
                     :text-decoration "underline"
                     ;:color (ycolors :blue10)
                     :font-weight 700}]
  [:.resume-item {:margin-bottom "60px"}
   [:.title {:text-decoration "underline"
             ;; :border-style "solid"
             ;; :border-width "0 0 3px 0"
             }]]
  [:.resume-content
   [:li
    [:.header {:font-weight "bold"}]
    [:.description {:font-weight 300
                    :padding-left "5px" }
     ]]]
  [:.resume-skills {:border-style "solid"
                    :border-width "3px"
                    :padding "5px"}]
  tep-charsheet)
