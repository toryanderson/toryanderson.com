(ns toryanderson.views.base
  "Container of the main view which facilitates CLJS app re-rendering and routing."
  (:require [toryanderson.routes :refer [current-view]]))

(defn main-view
  "Placeholder to render the main view"
  []
  (let [cfn @current-view]
    (cfn)))
