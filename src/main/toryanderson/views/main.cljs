(ns toryanderson.views.main
  "Our main view (index) page, including introduction and an aggregation of the XML feeds of my four primary blogs."
  (:require [reagent.core :as r]
            [clojure.string :as str]
            [toryanderson.views.styles :as style]
            [toryanderson.views.message :as messages]
            [toryanderson.xml :as xml]
            [ajax.core :as ax :refer [GET]]

            [goog.events.KeyCodes :as keycodes]
	    [goog.events :as gev])
  (:import [goog.events EventType KeyHandler]))

(defn get-value-from-change [e]
(.. e -target -value))

(def RSS-sites {:harmony {:url "https://harmony.toryanderson.com/index.xml"
                          :parent-url "https://harmony.toryanderson.com"
                          :image "images/cherubim.jpg"}
                :tech {:url "https://tech.toryanderson.com/index.xml"
                       :parent-url "https://tech.toryanderson.com"
                       :image "images/clojure.png"}
                :language {:url "https://language.toryanderson.com/index.xml"
                           :parent-url "https://language.toryanderson.com"
                           :image "images/wisdom_books.jpg"}
                :desiderata {:url "https://desiderata.toryanderson.com/index.xml"
                             :parent-url "https://desiderata.toryanderson.com"
                             :image "images/globe.jpg"} })

(def CONTACTS {:harmony [[:div.twitter
                          [:a {:href "https://twitter.com/WorldsEndless"}
                           [:i.fab.fa-twitter-square]
                           "@WorldsEndless"]]
                         [:div.mastodon 
                          [:a {:rel "me" :href "https://mastodon.online/@worldsendless"}
                           [:i.fab.fa-mastodon] 
                              "@worldsendless@mastodon.online"]]]
               :language [[:div.twitter
                           [:a {:href "https://twitter.com/CNarratology"}
                            [:i.fab.fa-twitter-square]
                            "@CNarratology"]]
                          [:div.mastodon 
                           [:a {:rel "me" :href "https://mastodon.social/@worldsendless"}
                              [:i.fab.fa-mastodon] 
                              "@worldsendless@mastodon.social"]]]
               :desiderata [[:div.twitter
                             [:a {:href "https://twitter.com/WorldsEndless"}
                              [:i.fab.fa-twitter-square]
                              "@WorldsEndless"]]
                            [:div.goodreads
                             [:a {:href "https://www.goodreads.com/user/show/4324162-tory-s-anderson"}
                              [:i.fab.fa-goodreads]
                              "@LinguaInfinitum"]]
                            [:div.bookwyrm
                             [:a {:href "https://bookwyrm.social/user/Endless-Reader"}
                              [:i.fab.fa-book]
                              "@Endless-Reader@bookwyrm.social"]]
                            [:div.mastodon 
                             [:a {:rel "me" :href "https://mstdn.social/@worldsendless"}
                              [:i.fab.fa-mastodon] 
                              "@worldsendless@mstdn.social"]]]
               :tech [[:div.twitter
                       [:a {:href "https://twitter.com/Endless_WebDev"}
                        [:i.fab.fa-twitter-square] 
                        "@Endless_WebDev"]]
                      [:div.github
                       [:a {:href "https://github.com/WorldsEndless"}
                        [:i.fab.fa-github-square] 
                        "@WorldsEndless"]]
                      [:div.gitlab
                       [:a {:href "https://gitlab.com/toryanderson"}
                        [:i.fab.fa-gitlab] 
                        "@ToryAnderson"]]
                      [:div.youtube
                       [:a {:href "https://www.youtube.com/channel/UCWeenNoykxfe1ywfAAOIOqw"}
                        [:i.fab.fa-youtube] 
                        "@WebDev"]]
                      [:div.mastodon 
                       [:a {:rel "me" :href "https://qoto.org/@worldsendless"}
                        [:i.fab.fa-mastodon] 
                        "@worldsendless@qoto.org"]]]}) 
(def RSS (r/atom {}))

(defn get-rss
  [kw]
  (when-let [url (get-in RSS-sites [kw :url])]
    (GET url {:handler #(swap! RSS assoc kw %)})))

(defn trunc
  "truncate `s` to length `n`"
  [n s]
  (subs s 0 (min (count s) n)))

(defn rss-box
  "A box containing the RSS content from one of the blogs"
  [rss-key] ;(def rss-key :tech)
  (let [amount 5
        s (rss-key @RSS)
        hick (xml/string->hickory s)
        parent-url (get-in RSS-sites [rss-key :parent-url])
        image (when-let [image-url (get-in RSS-sites [rss-key :image])]
                [:a {:href parent-url}
                 [:img.blog-image {:src image-url}]])
        items (->> hick xml/get-items)]
    (into [:div.rss-box ;; TODO make the RSS box collapsed until extended with a click
           image]
          (for [i (take amount items) ;(def i (nth items 2))
                :let [t (xml/get-title-from-item i)
                      l (str/trim (xml/get-link-from-item i))
                      d (xml/get-description-from-item i)
                      summary (if-not (str/blank? d)
                                (try (js/decodeURIComponent d)
                                     (catch js/Error e t))
                                t)]]
            [:a.rss-link {:href l :data-title summary} t]))))

(defn blog-box
  "The box that contains RSS feed for `(@RSS k)`"
  [k]
  (get-rss k)
  [:td.blog
   [rss-box k]])

(defn contact-box
  "Generate the contact-box for k"
  [k]
  (into [:td.contact] (CONTACTS k)))

(defn head-item
  "Headers for header"
  [k]
  [:th.title (-> k name str/capitalize)])

(defn tsa-quad
  "harmony, desiderata, tech, language"
  []
  (let [ks (keys RSS-sites)
        heads (map head-item ks)
        blogs (map blog-box ks)
        contacts (map contact-box ks)
        ]
    [:table.table
     [:thead
      (into [:tr] heads)]
     [:tbody 
      (into [:tr.rss-cols] blogs)
      (into [:tr.contact-cols] contacts)]]))

(defn toryanderson-title
  "Title Hero for the page"
  []
  [:section.hero
   [:div.container.box
    "I am a digital humanities scholar, software engineer, and computational psychonarratologist. Faith, technology, and language make up my life. Know me better in any of these domains."]])

#_(defn capture-key
  "Given a `keycode`, execute function `f` "
  [keycode-map]
  (let [key-handler (KeyHandler. js/document)
	press-fn (fn [key-press]
		   (when-let [f (get keycode-map (.. key-press -keyCode))]
                     (f)))]
    (gev/listen key-handler
		(-> KeyHandler .-EventType .-KEY)
		press-fn)))



(defn toryanderson
  "Front-page view"
  []
  #_(capture-key {keycodes/L #(js/alert "Luna Lovegood")
                keycodes/D #(js/alert "Dumbledore")
                keycodes/H #(js/alert "Hermione")
                keycodes/Y #(js/alert "harrY")
                keycodes/R #(js/alert "Ron")})
  [:div.container
   [toryanderson-title]
   [:div.body
    [tsa-quad]]])
