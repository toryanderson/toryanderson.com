(ns toryanderson.views.components.shared
  "Shared components that may be used on multiple views"
  (:require [reagent.core :as r]
            [goog.dom :as dom]
            [goog.string :as gstr])
  (:import goog.ui.AnimatedZippy))

(defn accordion [{:keys [header-text content-body]}]
  (let [header-id (gstr/createUniqueString)
        content-id (gstr/createUniqueString)]
    (r/create-class
     {:display-name "zippy"
      :reagent-render (fn [id] [:div.zippy-container
                                [:div {:id (str "wrap-" header-id)}
                                 [:h1.title.is-5 {:id header-id} header-text]
                                 [:div{:id content-id} content-body]]])
      :component-did-mount #(AnimatedZippy. (dom/$ header-id)
                                            (dom/$ content-id))})))
