(ns toryanderson.views.components.nav
  (:require [reagent.core :as r]
            [reitit.frontend.easy :as rfe]))

(def open-burger? (r/atom false))

(defn toggle-burger [& _]
  (swap! open-burger? not))

(defn burger
  ""
  []
  [:a.navbar-burger {:role "button"
                     :aria-label "menu"
                     :aria-expanded "false"
                     :on-click toggle-burger
                     :class (when @open-burger? "is-active")}
   [:span {:aria-hidden true}]
   [:span {:aria-hidden true}]
   [:span {:aria-hidden true}]])

(defn navbar []
  [:nav.navbar {:role "navigation" :aria-label "main nav"}
   [:div.navbar-brand
    [:a.navbar-item.title {:href (rfe/href :toryanderson.routes/toryanderson)} "ToryAnderson.com"]
    [burger]]
   [:div.navbar-menu {:class (when @open-burger? "is-active")}
    [:div.navbar-start
     [:a.navbar-item {:href (rfe/href :toryanderson.routes/cv)}
      "CV"]
     [:a.navbar-item {:href (rfe/href :toryanderson.routes/portfolio)}
      "Portfolio"]
     [:a.navbar-item {:href (rfe/href :toryanderson.routes/contact)}
      "Contact"]]]])
