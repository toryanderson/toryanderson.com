;; -*- eval: (rainbow-mode) -*-

(ns toryanderson.views.styles
  "Instead of loading a .css file, use css-in-cljs to load these straight into the page.
  See https://tech.toryanderson.com/2020/03/14/my-garden-css-has-ascended/"
  (:require [garden.core :as garden :refer [css] ]
            [garden.units :as u :refer [vh vw px em rem percent]]))

(def rss
  "Tiles for the front-page quad"
  [:.rss-box
   [:.title {:white-space :nowrap}]
   [:.blog-image {:height (vh 33)
                  :transition-property "transform" ;; only transition the size change; do the border immediately
                  :transition-duration "0.5s"
                  :transition-timing-function :ease
                  :transition-delay "0.25s"
                  :position :relative
                  :z-index 1}
    [:&:hover {:z-index 10
               :transform "scale(2)"
               :border-style :solid
               :border-width (px 2)
               :border-color "#00ff00"}]]
   [:a.rss-link {:display "block"
                 :margin-bottom (em 1)}
    [:&:hover {:background-color "#87cefa"}]]
   ["a[data-title]" ;; tooltip https://developer.mozilla.org/en-US/docs/Web/CSS/::after
    {:position "relative"
                                        ;:text-decoration nil
     :color "#00f"}
    ["&:hover::after" "&:focus::after"
     {:content "attr(data-title)"
      :position "absolute"
      :display :-webkit-box
      :-webkit-line-clamp 4
      :-webkit-box-orient :vertical
      :left (em 2)
      :top (px 24)
      :min-width (px 200)
      :overflow :hidden
      :border-width (px 1)
      :border-style "solid"
      :border-radius (px 10)
      :background-color "#FFFFCC"
      :color "#000"
      :font-size (px 14)
      :padding-top (em 1)
      :padding-left (em 1)
      :padding-right (em 1)
      :padding-bottom 0
      :z-index 1}]]])


(def contact
  "Contact front-page"
  [:.contact {:whitespace :nowrap}
   [:i {:margin-right (em 0.5)}]
   [:div :a
    {:display :inline-block
     :white-space :nowrap}]]
  )

(def nav
  "Navbar style"
  [:nav#tsa-nav
   [:.title {:margin-right (em 1)} ]
   [:.navbar-start
    [:.navbar-item {:font-size (rem 1.2)
                    :font-weight 900
                    :letter-spacing (em 0.1)}]]])

(def cv
  "Curriculum Vitae"
  (let [word-margins {:margin-left (em 0.5)
                      ;:margin-right (em 0.5)
                      }
        bgimg (fn [url]
                {:background-image (str "url(\"" url "\")")
                 :background-size :contain
                 :background-repeat :no-repeat
                 :background-position :center})]
    [:div.cv
     [:.cv-title {:margin-top (em 2)}]
     [:.byu-cv (bgimg "/images/byu.png")]
     [:.gt-cv (bgimg "/images/gt.png")
      ]
     [:ul
      [:li {:list-style :disc
            :margin-bottom (em 1)}
       [:.venue :.capacity
        [:&:after {:content "\". \""}]]
       [:.venue (assoc word-margins
                       :font-style :italic)]
       [:.year word-margins
        [:&:before {:content "\"(\""}]
        [:&:after {:content "\").\""}]]]]]))

(def contact-page
  [:div.contact-page
   [:p {:margin-left (em 2)}]])

(def footer
  [:footer.footer
   [:.copyright-me {:float :left}]
   [:.foot-codelink {:float :right}]])

(def portfolio
  [:.portfolio
   [:.description {:font-size (rem 1)}
    [:ul {:margin-left (em 1.5)
          :list-style "disc"}
     ]] 
   [:.image {:font-size (rem 1)}
    [:img {:max-height (vh 40)
           :width :unset
           :border-style :solid
           :border-width (px 2)
           :border-color :transparent}
     [:&:hover {:border-color "blue"}
      [:&:before {:position :absolute
                  :background-color "#2f4f4f"
                  :padding (em 0.5)
                  :color "#Adff2f"
                  :left (percent 30)
                  :top (percent 40)
                  :content "\"Full Size\""}]]]]])

(def accordions
  "Styles for the accordions "
  [:div.zippy-container
   [:.goog-zippy-header {:margin-bottom 0}
    [:&:hover {:background "#Add8e6"
               :cursor :pointer}]
    [:&:after {:margin-left (em 1)
               :text-decoration :none} ]
    [:&.goog-zippy-collapsed:after {:content "\"↓\""}]
    [:&.goog-zippy-expanded:after {:content "\"↑\""}]]
   [:.goog-zippy-content {:transition-property :all
                          :transition-duration "0.5s"
                          :transition-timing-function :ease
                          :transition-delay "0.5s"}]])

(def display-image
  [:.cntnr
   {:width "400px"
    :height "225px"
    :display "flex"
    :justify-content "center"
    :align-items "center"
    :flex-direction "column"
    :gap "10px"
    :border "1px solid black"}
   
   [:.display-image
    {:width "300px"
     :height "169px"
     :border "1px solid black"
     :background-position "center"
     :background-size "cover"}]])

(defn toryanderson
  "Our stylesheet"
  []
  (css
   [:body
    [:.message.is-success
     [:.content {:color "green"
                 :font-weight 600}]]
    [:table
     [:.title {:white-space "nowrap"}
      ]]
    nav
    accordions
    rss
    contact
    contact-page
    cv
    footer
    portfolio

    display-image]))

(defn clear-styles!
  "Remove existing style elements from the document <head>"
  []
  (let [styles (.getElementsByTagName js/document "style")
        style-count (.-length styles)]
    (doseq [n (range style-count 0 -1)]
      (.remove (aget styles (dec n))))))


(defn mount-style
  "Mount the style-element into the header with `style-text`, ensuring this is the only `<style>` in the doc"
  [style-text]
  (let [head (or (.-head js/document)
                 (aget (.getElementsByTagName js/document "head") 0))
        style-el (doto (.createElement js/document "style")
                   (-> .-type (set! "text/css"))
                   (.appendChild (.createTextNode js/document style-text)))]
    (clear-styles!)
    (.appendChild head style-el)))
