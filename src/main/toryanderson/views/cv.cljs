(ns toryanderson.views.cv
  "My Curriculum Vitae (resumé), featuring Closure accordions (zippys)"
  (:require [toryanderson.views.styles :as style]
            [toryanderson.views.components.shared :as sh]))

(defn section-title
  [s]
  [:h1.title.has-text-centered.cv-title s])

(def education
  [:div.container
   [section-title "Curriculum Vitae"]
   [:div.tile.is-ancestor.education-tiles   
    [:div.tile.is-parent
     [:div.tile.is-child.box.byu-cv
      [:h3.subtitle.has-text-centered.is-3 "BYU"]
      [:ul
       [:li.subtitle.is-5 "BA: Linguistics (2013)"]
       [:li "with: Honors"]
       [:li "Minor: Psychology"]
       [:li "Minor: Computer Science"]
       [:li "Minor: Linguistic Computing"]]]]
    [:div.tile.is-parent
     [:div.tile.is-child.box.gt-cv
      [:h2.subtitle.has-text-centered.is-3 "Georgia Tech"]
      [:ul
       [:li.subtitle.is-5 "MS: Digital Media (2015)"]
       [:li "Narrative Cognition"]
       [:li "Goal Reasoning"]]]]
    [:div.tile.is-parent
     [:div.tile.is-child.box.rd-cv
      [:h3.subtitle.has-text-centered.is-3 "R & D"]
      [:ul
       [:li "Idea Labs/Storylab"]
       [:li "BYU Digital Humanites"]
       [:li "Navy Research Lab (D.C.)"]
       [:li "Acxiom Data"]
       [:li "BYU Linguistics Lab"]]]]]])

(def pubs
  ;"Publications and Presentations"
  [{:title "An NEH Institute for Advanced Topics in Digital Humanities"
    :year "2019"
    :venue "Sustaining DH"
    :capacity "Workshop Participant"}
   {:title "Event tagging for narrative reconstruction"
    :year "2019"
    :venue "BYU Student Research Conference"
    :capacity "Co-Author"}
   {:title "Timeline survey"
    :year "2019"
    :venue "Digital Humanities: Utah 3 (Weber State University)"
    :capacity nil}
   {:title "Course design: DIGHT 340"
    :year "2018"
    :venue "Developing Javascript"
    :capacity nil}
   {:title "Natural language data visualization"
    :year "2016,17,18"
    :venue "BYU LING-485 Corpus Linguistics"
    :capacity "Guest lectures"}
   {:title "Demonstrating Fairy Tale TV"
    :year "2018"
    :venue "Digital Humanities: Utah 2, Utah State University"
    :capacity "Technical demonstration"}
   {:title "Designing Fairy Tale TV"
    :year "2017"
    :venue "Digital Humanities: Utah, Utah Valley University"
    :capacity "Technical discussion"}
   {:title "Using Watson for constructing cognitive assistants"
    :year "2016"
    :venue "Adv. Cogn. Syst. 4, 1-16."
    :capacity nil}, 
   {:title "From episodic memory to narrative in a cognitive architecture"
    :year "2015"
    :venue "Workshop on Computational Models of Narrative"
    :capacity nil} 
   {:title "Goal reasoning and narrative cognition"
    :year "2015"
    :venue "2015 Annual Conference on Advances in Cognitive Systems: Workshop on Goal Reasoning"
    :capacity nil} 
   {:title "Thinking in stories"
    :year "2015"
    :venue "Master's Thesis, Georgia Institute of Technology"
    :capacity nil}
   {:title "NI-Discover History: meta-narrative for explanation bounding"
    :year "2014"
    :venue "Technical Report, Navy Research Lab"
    :capacity nil}
   {:title "Syntax stories: episodic memory for natural language processing with Soar"
    :year "2013"
    :venue "Honors Thesis, Brigham Young University"
    :capacity nil}
   {:title "Semantic memory for syntactic disambiguation"
    :year "2013"
    :venue "Proceedings of the 12th International Conference on Cognitive Modeling (ICCM 2013)"
    :capacity nil}
   ])

(def publications
  [:div.publications
   [section-title "Publications"]
   (into [:ul.publication-list]
         (for [p (sort-by > :year pubs) :let [{:keys [title year venue capacity]} p]]
           [:li.publication
            [:span.ptitle title]
            [:span.year year]
            [:span.venue venue]
            (when capacity [:span.capacity capacity])]))])

(def work
  [:div.work-experiences
   [section-title "Work & Experience"]
   [:div.tile.is-ancestor.experience-tiles.is-vertical.content
    [:div.tile.is-child.box
     [sh/accordion {:header-text "Devops & Deployment (2019+)"
                    :content-body [:ul
                                   [:li "Linux"]
                                   [:li "CI/CD with Jenkins"]
                                   [:li "Full server configuration"
                                    ;; TODO: nested bullets
                                    [:ul 
                                     [:li "Apache"]
                                     [:li "Wildfly"]
                                     [:li "NginX"]]]
                                   [:li "SystemD"]
                                   [:li "Linux command line (Bash)"]]}]]
    [:div.tile.is-child.box
     [sh/accordion {:header-text "Data Modeling & Database Design"
                    :content-body [:ul
                                   [:li "SQL (Postgres, MySQL, SQLite)"]
                                   [:li "NoSQL (Postgres, Datalog, Mongo)"]
                                   [:li "SQL visualizations (ERD)"]]}]]
[:div.tile.is-child.box
     [sh/accordion {:header-text "BYU ODH Senior Web Application Engineer & Research Partner (2018+)"
                    :content-body
                    [:ul 
                     [:li [:strong "Tech Spec:"] " LAMP, Node.js, d3js, Polymer, Wordpress, Clojure & Clojurescript, Server Administration, R statistics, Mongo DB, MySQL, Postgres, Docker"]
                     [:li "Office councillor for project intake, technology support, and project deployment matters"]
                     [:li "Developed and maintained a variety of programs for purposes including data visualization, text analysis, stylometrics"]
                     [:li "Developed back-end web-services interacting with University web services"]]}]]

    [:div.tile.is-child.box
     [sh/accordion {:header-text "BYU Web Admin Committee (2016-2020)"
                    :content-body
                    [:ul 
                     [:li "Participated in re-organization of university-wide Web Community structure"]
                     [:li "BYU.edu: Involved in discussions of planning and technical development of official BYU web presence and identity"]
                     [:li "Involved in discussions for selection and adoption of University-wide Digital Asset Management system"]]}]]
    [:div.tile.is-child.box
     [sh/accordion {:header-text "BYU Humanities College Webmaster (2015-2018)"
                    :content-body [:ul
                                   [:li "Developed plugins and sub-themes for Wordpress which interacted with the BYU core site and services"]
                                   [:li "Planned, customized, trained, and implemented college-wide adoption of web framework"]
                                   [:li "Server and DNS management for over 100 personal, organization, and web-app sites"]
                                   [:li "Hiring, training, and supervision of Humwebhelp student web service team (2018)"]
                                   [:li "Sole technical worker implementing, customizing, and rolling-out custom Wordpress Theme to 8 Humanities departments (2016)"]]}]]

    [:div.tile.is-child.box
     [sh/accordion {:header-text "Navy Research Laboratory: Student Research Contractor (2014-15)"
                    :content-body
                    [:ul 
                     [:li [:strong "Tech Spec:"] " VM, Linux, LaTeX, Emacs/Orgmode, BibTeX"]
                     [:li "Brought insights from computational narrative to planning & goal reasoning"]
                     [:li "Helped refine traditional planning methods for special scenarios"]
                     [:li "Created extensive literature reviews in multi-agent plan recognition and computational narrative"]]}]]

    [:div.tile.is-child.box
     [sh/accordion {:header-text "Acxiom Corp.: Computational Linguist (2012-13)"
                    :content-body
                    [:ul 
                     [:li [:strong "Tech Spec:"] " Java, Perl, Mojolicious, Linux, Bash shell"]
                     [:li "Web-crawled to produce 4-million word topical corpus"]
                     [:li "Designed & developed system to parse JSON arrays, extract twitter messages, and lexically analyze over 180-million messages for sentiment & topic in reasonable time. Utilized a dynamic web framework with AJAX to create a cloud-based launcher for this system."]]}

      [:div.tile.is-child.box
       [sh/accordion {:header-text "Soar Artificial Intelligence Developer (2009-13)"
                      :content-body
                      [:ul 
                       [:li [:strong "Tech Spec:"] " Soar, LaTeX, syntactic parsing"]
                       [:li "Undergraduate thesis: implementing episodic memory for language processing"]
                       [:li "Work presented at Soar Workshop 33, June 2013"]]}]]]]]])

(defn render
  "Render the CV view"
  []
  [:div.container.cv
   [:div.body
    education
    work
    publications
    
    ]]
  )
