(ns toryanderson.views.contact
  "Contact page"
  (:require [reitit.frontend.easy :as rfe]))

(defn render
  "Render the contact view with a randomized greeting from four natural languages with which I have experience"
  []
  (let [greetings ["Salutations"
                   "Salut"
                   "您好"
                   "안녕하세요"]]
    [:div.contact-page.content
     [:h1.title.has-text-centered (rand-nth greetings)]
     [:p 
      "I recommend contacting me via any of the media shown on "
      [:a {:href (rfe/href :toryanderson.routes/toryanderson)} "the front-page"]
      " depending on which capacity you want to talk."]
     [:p "If you're really trying to be generic, you can"
      [:a {:href "mailto:mail@toryanderson.com"}
       " just email me here. I'll probably answer, because I like you."] ""]]))

