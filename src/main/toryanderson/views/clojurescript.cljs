(ns toryanderson.views.clojurescript
  "Portfolio page showing some of my Clojurescript work"
  (:require [reagent.core :as r]))

;; TODO put each item in its own component ns, to allow for easier sharing

(defn change-read
  "Called on the change event of a file input, operate on the selected file"
  [clicked UPLOADED-IMAGE]
  (let [reader (js/FileReader.)
        set-image (fn [loade]
                    (reset! UPLOADED-IMAGE (-> loade
                                                    .-target
                                                    .-result))
                    ;; (js/console.log (str "upped is the image data at this point in the thread: >>>"))
                    ;; (js/console.log @uploaded-image)
                    )
        file (first (array-seq (.. clicked -target -files)))
        image-path (-> clicked .-target .-value)]
    ;; (js/console.log (str "Image file is >>> "))
    ;; (js/console.log file)
    (.addEventListener reader "load" set-image)
    (.readAsDataURL reader file) ;; this triggers the "onload" event
    ;; (js/console.log "image path is >>>")
    ;; (js/console.log image-path)
    ))

(defn image-selector
  "an image-selection element that shows the image you've chosen.

  This is a form-2 reagent component so we can avoid global state.
  https://github.com/reagent-project/reagent/blob/master/doc/CreatingReagentComponents.md#form-2--a-function-returning-a-function"
  []
  (let [UPLOADED-IMAGE (r/atom nil)
        bg-image-style (fn []
                             (if-let [i @UPLOADED-IMAGE]
                               {:background-image
                                (str "url(" i ")")}
                               {:background-color "red"}))]
    (fn []
      [:div.cntnr
       [:input {:type "file"
                :id "image-in"
                :on-change #(change-read % UPLOADED-IMAGE)
                :accept "image/*"}]
       [:div.display-image {:style (bg-image-style)}]])))

(def glowing-icons
  [:div "icons here"])

(defn render
  "Render the CV view"
  []
  [:div.container.portfolio
   [:div.body
    [:h1.title.has-text-centered.portfolio-title.is-1
     "Clojurescript"]
    [:div.tile.is-vertical
     [:div.tile.is-parent.is-vertical
      [image-selector]
      glowing-icons]]
    
    ]]
  )
