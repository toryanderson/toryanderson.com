(ns toryanderson.views.portfolio
  "Portfolio page showing some of my work"
  (:require [reitit.frontend.easy :as rfe]))

(defn port-child
  "One portfolio item"
  [{:keys [title site subtitle year description-bullets image]}]
  (let [sub (cond-> subtitle
              year (str " (" year ")"))
        image-path (when-let [f (:file image)]
                     (if (and
                          (not (re-find #"^//" f))
                          (#{"/"} (first f)))
                       (str site f)
                       f))] 
    [:article.tile.is-child
     [:h2.title.has-text-centered.is-3 title]
     [:h3.subtitle.has-text-centered.is-4 sub]
     [:div.columns
      [:div.description.column
       (into [:ul]
             (map #(vector :li %) description-bullets))]
      [:div.image.column
       (when image
         [:a {:href image-path}
          [:img {:src image-path}]])
       (when site [:a {:href site}
                   (:caption image title)])]]]))

(def portfolio-items
  [
   ;; humforms
   ;; yvideo
   {:site (rfe/href :toryanderson.routes/clojurescript)
    :title "Clojurescript Samples"
    :subtitle "Jewels of Clojurescript"
    :description-bullets ["Garden CSS"
                          "Image Upload Preview"]
    :image {:file "https://toryanderson.com/images/clojure.png"}
    }   
     
   {:site "https://pangrammer.byu.edu"
    :title "Pangrammer"
    :subtitle "Digital Humanities: Pangrammer Single-Page Game"
    :year "2020"
    :description-bullets ["Clojurescript"
                          "shadowCLJS"
                          "Single-page Application"]
    :image {:file "https://images.toryanderson.com/pangrammer.png"
            }}
{ ;:site "#"
    :title "Humforms PDF Generation"
    :subtitle "Official PDF generation from data at 3rd Party Site"
    :year "2019"
    :description-bullets ["Clojure"
                          [:span
                           [:a {:href "https://www.formstack.com/"} "Formstack"] " and "
                           [:a {:href "https://build.kuali.co/"} "Kuali"]] 
                          [:a {:href "https://github.com/clj-pdf/clj-pdf"} "clj-pdf"]]
    ;:image {:file "https://images.toryanderson.com/pangrammer.png"}
    }
   {:site "https://fttv.byu.edu"
    :title "Fairytale TV"
    :subtitle "Digital Humanities: Folklore"
    :year "2019"
    :description-bullets ["Fairytales"
                          "Database Design"
                          "Postgres"
                          "Clojure"
                          "Clojurescript"
                          "d3js"]
    :image {:file "https://images.toryanderson.com/tooltips.gif"
            :caption "Fairytale TV"}}
   {:site "http://apps.humanities.byu.edu/maladroit"
    :title "Maladroit"
    :subtitle "Digital Humanities: Text Clustering"
    :year "2016"
    :description-bullets ["Clojure"
                          "Java"
                          "Gephi"
                          "Clojurescript"]
    :image {:file "https://images.toryanderson.com/maladroit.png"
            :caption "Maladroit"}}   

   {:title "Storymark"
    :subtitle "Design Fiction"
    :year "2014"
    :description-bullets ["Speculative Design"
                          "Image editing"
                          "Gimp"]
    :site "http://storymark.toryanderson.com"
    :image {:file "/wp-content/uploads/2014/03/storymark_final.jpg"}}
      
      #_(port-child {:title "Photokite"
                     :subtitle "SVG Mockup"
                     :description-bullets [""]
                     :image {:site "http://storymark.toryanderson.com"
                             :file "/wp-content/uploads/2014/03/storymark_final.jpg"
                             }})
   {:title "LitLeft"
    :year "2014"
    :subtitle "Exploratory Course Work"
    :description-bullets ["Intellectual Property"
                          "Literature"
                          "Open Source"
                          "Digital Media"]
    :site "http://litleft.toryanderson.com"
    :image {:file "/sites/default/files/logo_1.png"}}
                                        
   {:title "Life Stories"
    :year "2014"
    :subtitle "Interactive Digital Experience"
    :description-bullets ["Web Application"
                          "three.js"
                          "Javascript"]
    :site "https://toryanderson.com/LifeStories"
    :image {:file "https://images.toryanderson.com/LifeStories.png"}}
   ])

(defn render
  "Render the CV view"
  []
  [:div.container.portfolio
   [:div.body
    [:h1.title.has-text-centered.portfolio-title.is-1
     "Portfolio"]
    [:div.tile.is-vertical
     (into 
      [:div.tile.is-parent.is-vertical]
      (map port-child portfolio-items))]
    

    
    ]]
  )
