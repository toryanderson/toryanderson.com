(ns toryanderson.xml
  "XML functions for parsing the RSS feed info"
  (:require [clojure.string :as str]
            [hickory.core :as h]
            [hickory.select :as s]))

(def string->hickory (comp h/as-hickory h/parse))

(def get-items
  "Get item elements from root hickory parse"
  (partial s/select (s/tag :item)))

(def get-titles
  "Get title elements from root hickory parse"
  (partial s/select
           (s/child 
            (s/tag :item)
            (s/tag :title))))

(defn get-title-from-item
  "Given a hickory product of `get-items`, get the title string of that item"
  [i]
  (-> i :content second :content  first))

(defn get-link-from-item
  "Given a hickory product of `get-items`, get the relative link portion of that item "
  [i & [parent-url]]
  (let [parentize (if parent-url #(str parent-url %)
                      identity)]
    (-> (get-in i [:content 4])
        (str/split #"\\")
        first
        parentize)))

(defn get-description-from-item
  "Get the description that goes with an item"
  [i]
  (get-in i [:content 9 :content 0]))

(comment
  (def i {:type :element, :attrs nil, :tag :item, :content ["\\n      " {:type :element, :attrs nil, :tag :title, :content ["Finding your time"]} "\\n      " {:type :element, :attrs nil, :tag :link, :content nil} "https://harmony.toryanderson.com/2020/05/17/finding-your-time/\\n      " {:type :element, :attrs nil, :tag :pubdate, :content ["Sun, 17 May 2020 00:00:00 +0000"]} "\\n      \\n      " {:type :element, :attrs nil, :tag :guid, :content ["https://harmony.toryanderson.com/2020/05/17/finding-your-time/"]} "\\n      " {:type :element, :attrs nil, :tag :description, :content ["My personal study time is vital to my mental health, a key part of gaining my focus and orientation for the day. I learned this long ago, perhaps even before my mission, but it&rsquo;s been a sacred time ever since then. Do you have a time of your own?\\n Improving Personal Study\\nFind a time that works for you. It is often easiest to learn when you can study the scriptures without being interrupted."]} "\\n    "]})
  (def i {:type :element, :attrs nil, :tag :item, :content ["\n      " {:type :element, :attrs nil, :tag :title, :content ["Clojure app setup for Auto-deploy with raw systemd"]} "\n      " {:type :element, :attrs nil, :tag :link, :content nil} "/2020/09/04/clojure-app-setup-for-auto-deploy-with-raw-systemd/\n      " {:type :element, :attrs nil, :tag :pubdate, :content ["Fri, 04 Sep 2020 00:00:00 +0000"]} "\n      \n      " {:type :element, :attrs nil, :tag :guid, :content ["/2020/09/04/clojure-app-setup-for-auto-deploy-with-raw-systemd/"]} "\n      " {:type :element, :attrs nil, :tag :description, :content ["With this, as much for my own notes as for anyone else&rsquo;s instruction, I&rsquo;m detailing setting up a deployment process for a long-running Clojure application. As some background, we utilize Apache on Ubuntu Linux servers and I will be deploying uberjar files. We deploy both staging versions, for our clients to see and play with, and production versions once the staging version is approved. You will need to have full admin privileges on the server."]} "\n    "]}))
