(ns toryanderson.routes
  (:require [reagent.core :as r]
            [reitit.frontend :as rf]
            [reitit.frontend.easy :as rfe]            
            [toryanderson.views.main :as main]
            [toryanderson.views.contact :as contact]
            [toryanderson.views.cv :as cv]
            [toryanderson.views.clojurescript :as cljs]
            [toryanderson.views.portfolio :as portfolio]))

(defn default-view
  "To show before routes are loaded"
  []
  [:h1 "Loading..."])

(defonce current-view (r/atom default-view) )

(def routes
  (rf/router
   ["/"
    [""
     {:name ::toryanderson
      :view #'main/toryanderson}]
    ["cv/"
     {:name ::cv
      :view #'cv/render}]
    ["contact/"
     {:name ::contact
      :view #'contact/render}]
    ["clojurescript/"
     {:name ::clojurescript
      :view #'cljs/render}]
    ["portfolio/"
     {:name ::portfolio
      :view #'portfolio/render}]]))

(defn init-routes!
  "Start the routing"
  []
  (rfe/start! routes
              (fn [m]
                (reset! current-view (get-in m [:data :view])))
              {:use-fragment false}))
