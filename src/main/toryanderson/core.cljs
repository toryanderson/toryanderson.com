(ns toryanderson.core
  (:require [reagent.core :as r]
            [toryanderson.views.styles :as style]
            [toryanderson.routes :as routes]
            [toryanderson.views.base :as base]
            [toryanderson.views.components.nav :as nav]
            [toryanderson.views.message :as message]))

(defn mount-components []
  (r/render-component [#'nav/navbar] (.getElementById js/document "tsa-nav"))
  (r/render-component [#'base/main-view] (.getElementById js/document "app")) ;
  (r/render-component [message/message-view] (.getElementById js/document "message")))

(defn init! []
  (style/mount-style (style/toryanderson))
  (routes/init-routes!)
  (mount-components))
