(ns com.toryanderson.navbar
  (:require
   [reagent.core :as r]
   [reagent.session :as session]
   [com.toryanderson.routes :as routes]))

(defn nav-link [uri title page collapsed?]
  [:li.nav-item
   {:class (when (= page (session/get :page)) "active")}
   [:a.nav-link
    {:href uri
     :on-click #(reset! collapsed? true)} title]])

(defn social-media [] ;; have a slider button for personal vs. professional mode
  [:div.dropdown.nav-item
   [:a.nav-link.dropdown-toggle {:data-toggle "dropdown" :href "#" :data-target "#"} "Follow Me"
    [:b.caret]]
   [:ul.dropdown-menu {:role "menu"}
    [:li.facebook [:a {:href "https://www.facebook.com/profile.php?id=100008183975154"} "Facebook"]]
    [:li.twitter [:a {:href "https://twitter.com/Endless_WebDev"}"Twitter"]]
    ;[:li [:a "Quora"]]
    [:li.github [:a {:href "https://github.com/WorldsEndless"} "GitHub"]]]])

(defn writings [] 
  [:div.dropdown.nav-item
   [:a.nav-link.dropdown-toggle {:data-toggle "dropdown" :href "#" :data-target "#"} "Writings"
    [:b.caret]]
   [:ul.dropdown-menu {:role "menu"}    
    [:li [:a {:href "http://harmony.toryanderson.com"} "Harmony"]]
    [:li [:a {:href "http://language.toryanderson.com"} "Language"]]
    [:li [:a {:href "http://tech.toryanderson.com"} "Tech"]]
    [:li [:a {:href "http://desiderata.toryanderson.com"} "Desiderata"]]]])

(defn navbar []
  (let [collapsed? (r/atom true)]
    (fn []
      [:nav.navbar
       [:button.navbar-toggler.hidden-sm-up
        {:on-click #(swap! collapsed? not)} "☰"]
       [:div.collapse.navbar-toggleable-xs
        (when-not @collapsed? {:class "in"})
        [:a.navbar-brand {:href "#/"} "ToryAnderson.com"]
        [:ul.nav.navbar-nav
         [nav-link "#/" "Home" :home collapsed?]
         [nav-link "#/resume" "Resumé" :resume collapsed?]
         [nav-link "#/publications" "Publications" :publications collapsed?]
         [writings]         
         [social-media]
         ]]])))
