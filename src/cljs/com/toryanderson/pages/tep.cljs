(ns com.toryanderson.pages.tep)

(def skill-inventory-count 20)

(defn make-input [pc]
  (into [:div.row.personaldetails [:h2 "Personal Details"]]
        (for [[k v] pc]
          (let [name (name k)
                name-of-input (str "input_" name)]
            [:div.col.md-4.detail {:class (str "detail_" name)}
             [:label {:for name-of-input} name]
             [:input {:name name-of-input
                      :class name-of-input}]]))))

(def personal-details
  (let [pdata {:Name ""
               :Race ""
               :Age ""
               :Weight ""
               :Height ""}]
    (make-input pdata)))

(def points
  (into [:div.row.points]
        (for [p ["CP" "VP"]]
          (let [name-of-input (str "input_" p)]
            [:div {:class p}
             [:label {:for name-of-input} p]
             [:input {:class p
                      :name name-of-input}]]))))

(def attributes
  (let [categories [["Physical" "Phys"]
                    ["Mental" "Ment"]
                    ["Elemental" "Element"]]
        attributes ["Strength" "Agility" "Endurance" "Charm"]
        table-headers (into [:div.row.headers]
                            (for [a (cons "" attributes)]
                             [:div.col a]))
        ]
    (into [:div.table.attributes table-headers]
          (for [[c cs] categories]
            (into [:div.row {:class (str "row_" c)} c]
                  (for [a attributes] [:div.col.md-3 {:class (str c "_" a)} ;(str c " " a)
                                       ]))))))

(def health
  [:div.health
   [:h2 "Health"]
   [:div.bonuses [:h3 "Bonuses"]]
   [:div.penalties [:h3 "Penalties"]]
   [:div.wear [:h3 "Depletion"]]])

(def skills
  (into [:div.skills [:h2 "Skills"]]
        (for [i (range skill-inventory-count)]
          [:div.row.skills {:class (str "skill_" i)}
           [:div.col-md-5.skill-name "_"]
           [:div.col-md-3.skill-factor "1d6"]]
          )))

(def inventory
  (into [:div.inventory [:h2 "Inventory"]]
        (for [i (range skill-inventory-count)]
          [:div.row.inventory {:class (str "inventory_" i)}
           [:div.col-md-5.item-name "_"]
           [:div.col-md-3.item-line ""]]
          )))

(defn charsheet []
  [:div.container.tep-charsheet
   [:div.row
    [:h1.col-md-12.title
     "The Elemental Project 2"]]
   personal-details
   points
   attributes
   health
   skills
   inventory
   ])
