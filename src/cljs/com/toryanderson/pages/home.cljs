(ns com.toryanderson.pages.home)

(defn story-types ;; TODO
  "A sliding list of different types of stories"
  []
  [:ul
   [:li "Web sites"]
   [:li "Fairy tales"]
   [:li "Life stories"]
   [:li "Movies, books, or music"]])

(def professional-introduction [:p "Tory Anderson, "
                                [:span.computational "Computational"]
                                [:span.psychology " Psycho"]
                                [:span.narratology "narratologist"]
                                ". What does that mean? It means I get my kicks by investigating the way that stories work "
                                [:span.underline "in"]
                                " and "
                                [:span.underline "on"]
                                " the human mind, even if those "
                                [:a {:href "#/tepsheet"}" stories "]
                                " are "
                                [:span#story-types [story-types]]])

(def personal-brief [:p "In my off time I spend my time thinking and working on matters of faith, family, and technology."])

(defn gen-page []
  [:div.container
   [:div.row>div.col-sm-12.portrait
    [:div.portrait-box]]
   [:div.row>div.col-sm-12.professional-intro
    professional-introduction]
   [:div.row>div.col-sm-12.personal-brief
    personal-brief
    ]])
