(ns com.toryanderson.routes
  (:require
   [reagent.session :as session]
   [secretary.core :as secretary :include-macros true]
   [com.toryanderson.pages.home :as home]
   [com.toryanderson.pages.bio :as bio]
   [com.toryanderson.pages.resume :as resume]
   [com.toryanderson.pages.publications :as publications]
   [com.toryanderson.pages.tep :as tep]))

(def pages
  {:home #'home/gen-page
   :bio #'bio/gen-page
   :resume #'resume/gen-page
   :publications #'publications/gen-page
   :portfolio #'bio/gen-page
   :writings #'bio/gen-page
   :tepsheet #'tep/charsheet})

;; -------------------------
;; Routes
(secretary/set-config! :prefix "#")

(secretary/defroute "/" []
  (session/put! :page :home))

(secretary/defroute "/bio" []
  (session/put! :page :bio))

(secretary/defroute "/resume" []
  (session/put! :page :resume))

(secretary/defroute "/portfolio" []
  (session/put! :page :portfolio))

(secretary/defroute "/publications" []
  (session/put! :page :publications))


(secretary/defroute "/writings" []
  (session/put! :page :writings))

(secretary/defroute "/tepsheet" []
  (session/put! :page :tepsheet))
