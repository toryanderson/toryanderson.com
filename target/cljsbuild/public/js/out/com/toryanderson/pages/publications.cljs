(ns com.toryanderson.pages.publications)

(defn publication [args]
  (into [:li]
        (for [[k v] (partition 2 args)] [:span {:class (name k)} v])))

(defn gen-page []
  [:div.container.publications
   [:div.jumbotron
    [:h1 "Publications"]]
   [:div.row
    [:div.col-md-12
     [:ul.publications
      [publication [:title "Thinking in Stories"
                    :publisher "MS Thesis, Georgia Institute of Technology"
                    :date "2015"]]
      [publication [:title "Thinking in Stories"
                    :publisher "Computational Models of Narrative Workshop at Knowledge-Based AI Conference"
                    :date "2015"]]
      [publication [:title "Episodic Memory for Gardenpath Sentences"
                    :publisher "Poster at BRIMS"
                    :date "2014"]]
      [publication [:title "Syntax Stories"
                    :publisher "Honors Thesis, Brigham Young University"
                    :date "2013"]]]]]])
