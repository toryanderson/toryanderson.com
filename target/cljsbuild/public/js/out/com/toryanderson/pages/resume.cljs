(ns com.toryanderson.pages.resume)

;; TODO Skills carousel

(defn byu-details [extra-map]
  (let [defaults {:employer "College of Humanities, Brigham Young University"
                  :start-time "2015"
                  :end-time "Present"}]
    (merge defaults extra-map )))

(defn resume-item [content-map]
  (let [{:keys [title employer description start-time end-time skills references]} content-map]
    [:div.resume-item
     [:h1.title title]
     [:h2.employer employer]
     (when start-time
       [:h3.time start-time
        (when end-time (str  "-" end-time))])
     [:div.resume-content
      description]
     (when skills
       (into [:div.resume-skills
              [:h3 "Skills"]
              (for [s skills] [:div.skill (s :name)])]))
     (when references
       [:div.reference
        references])]))

(defn resume []
  [:div.resume
   [resume-item (byu-details {:title "Senior Web Application Developer"
                              :description [:div "Oversaw development and maintainance of applications for the Deanery and each of the 8 departments within humanities"
                                            [:ul
                                             [:li 
[:span.header "Humfunding"]
                                              [:span.description "Developed and maintained an application that integrated and extended a proprietary 3rd-party solution for college-wide faculty funding applications"]]
                                             [:li 
[:span.header "TurboTenure"]
                                              [:span.description "Developed a system automating much of the tenure-application process for advancing faculty"]]
                                             [:li 
[:span.header "Humplus-Funding"]
                                              [:span.description "Developed a system facilitating student applications for internship, study-abroad, and experiential-learning funding"]]]]
                              :skills [{:name "Clojure"} {:name "ClojureScript"} {:NAME "PHP"} {:name "Python"} {:name "MongoDB"} {:name "MySQL"} {:name "PostGresql"}]})]
   [resume-item (byu-details {:title "Software Project Manager"
                              :employer "Office of Digital Humanities, Brigham Young University"
                              :description [:div "Oversaw large projects and teams of student developers and designers"
                                            [:ul
                                             [:li 
[:span.header "Y-Video project"]]
                                             [:li 
[:span.header "Testing-lab Signups"]
                                              [:span.description "Instructed a student in learning PHP and MySQL to build an online interface for testees to sign up for in-lab tests"]]
                                             [:li 
[:span.header "Foreign Language Acquisition Test (FLATS)"]
                                              [:span.description "Maintained and re-designed a nation-wide accredited audio-text language test"]]]]})]
   [resume-item (byu-details ;; TODO screenshots for the sites
                 {:title "Web Master & Computer Support Representative"
                  :description [:div "Maintained, developed, designed over 80 websites for college, departmental, and research organizations, including:"
                                [:ul
                                 [:li "The primary college website, " [:a {:href "http://humanities.byu.edu"} "humanities.byu.edu"]]
                                 [:li "Each of the 8 departments websites and their 20+ subsidiary websites"]
                                 [:li "Dozens of websites for research organizations, conferences, publiations, and individual professors"]
                                 [:li "One of two network administrators responsible for hosting, DNS management, and server oversight"]
                                 [:li "Personally coordinated with the communications team to develop and uniformly migrate the entire college to a new website platform and new servers"]]]
                  :skills [{:name "Apache"} {:name "Wordpress"} {:name "NginX"} {:name "Bash"} {:name "Linux"}]})]
   [resume-item (byu-details
                 {:title "Digital Humanities R&D"
                  :employer "Office of Digital Humanities, Brigham Young University"
                  :description [:div "Provided application development to complement researcher work."
                                [:ul
                                 [:li 
[:span.header "English Genome Project"]
                                  [:span.description "Stylometric analysis and comparison of essays"]]
                                 [:li 
[:span.header "Fairy Tale TV"]
                                  [:span.description "Compilation and visualization of fairy tale references on popular TV"]]]]})]
   [resume-item 
    {:title "Contract Researcher: Narrative & AI"
     :employer "Navy Research Lab"
     :start-time "2014"
     :end-time "2015"
     :description [:div "Researched cognitive narrative solutions for team-member AI"]}]
   [resume-item
    {:title "Data Analyst"
     :employer "Axiom Data"
     :start-time "2013"
     :end-time "2015"
     :description [:div "Authored software for basic sentiment analysis of gigabytes of Twitter messages"]}]
   ])

;; TODO Georgia Tech work
;; TODO Axiom work
;; TODO NRL work

(defn gen-page []
  [:div.container.resume
   [:div.jumbotron
    [:h1 "Resumé"]]
   [:div.row>div.col-sm-12
    [resume]]])

