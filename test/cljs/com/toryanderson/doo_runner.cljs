(ns com.toryanderson.doo-runner
  (:require [doo.runner :refer-macros [doo-tests]]
            [com.toryanderson.core-test]))

(doo-tests 'com.toryanderson.core-test)

