#!/usr/bin/env bash
npx shadow-cljs release :app &&
rsync -av --no-perms target/ torys:www/toryanderson
echo "Deployment process finished. Visit https://toryanderson.com to verify."
