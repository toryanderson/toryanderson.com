(ns com.toryanderson.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [com.toryanderson.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[com.toryanderson started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[com.toryanderson has shut down successfully]=-"))
   :middleware wrap-dev})
