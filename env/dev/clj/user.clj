(ns user
  (:require [mount.core :as mount]
            [com.toryanderson.figwheel :refer [start-fw stop-fw cljs]]
            com.toryanderson.core))

(defn start []
  (mount/start-without #'com.toryanderson.core/repl-server))

(defn stop []
  (mount/stop-except #'com.toryanderson.core/repl-server))

(defn restart []
  (stop)
  (start))


