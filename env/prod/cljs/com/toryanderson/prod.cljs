(ns com.toryanderson.app
  (:require [com.toryanderson.core :as core]))

;;ignore println statements in prod
(set! *print-fn* (fn [& _]))

(core/init!)
